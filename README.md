# Allemann_Chappuis_Stouder

**SubscriptionTracker** : 

Projet permettant de lister nos abonnements (leasing, abonnement téléphone, TV, etc.) et leur coût, afin d'avoir un récapitulatif où partent nos dépenses.

[Accès au wiki](https://gitlab-etu.ing.he-arc.ch/isc/2021-22/niveau-2/2255-1-dotnet/projets/allemann_chappuis_stouder/-/wikis/home)

![Logo](https://www.he-arc.ch/wp-content/themes/he-arc/static/images/logo-he-arc.svg)
