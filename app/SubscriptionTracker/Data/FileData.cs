﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SubscriptionTracker.Data
{
    /// <summary>
    /// Represents the root of the datas which will be serialized (subscriptions)
    /// </summary>
    [Serializable]
    [XmlRoot("Data")]
    public class FileData
    {
        #region Property
        /// <summary>
        /// Path of the opened xml file that contains the subscription
        /// </summary>
        [XmlIgnore]
        public string XmlPath { get; set; }

        /// <summary>
        /// Is the last version of the data are saved
        /// </summary>
        [XmlIgnore]
        public bool IsSaved { get; set; } = true;

        /// <summary>
        /// The version of the program used to create the file (for future version compatibility)
        /// </summary>
        public string AppVersionCode { get; set; } = "v1.0.0";


        /// <summary>
        /// The subscriptions collection that contains this data
        /// </summary>
        [XmlArray("Subscriptions")]
        [XmlArrayItem("Subscription")]
        public ObservableCollection<Subscription> Subscriptions { get; set; } = new ObservableCollection<Subscription>();
        #endregion Property

    }
}
