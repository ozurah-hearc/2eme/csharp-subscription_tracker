﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Net.Http;
using System.Web;

namespace SubscriptionTracker.Data
{
    [Serializable]
    public class Subscription : INotifyPropertyChanged
    {

        #region Fields for Property
        private string name;
        private string category;
        private decimal price;
        private BillingPeriod period = BillingPeriod.Unspecified;
        private string address;
        private DateTime startDate = DateTime.Today;
        private bool isActive = true;
        private bool isAutomaticDebit;
        #endregion Fields for Property

        #region Enum
        /// <summary>
        /// At which frequence the subscription will be payed
        /// </summary>
        public enum BillingPeriod
        {
            [XmlEnum("Year")]
            Year,
            [XmlEnum("Month")]
            Month,
            [XmlEnum("Week")]
            Week,
            [XmlEnum("Unspecified")]
            Unspecified
        }
        #endregion Enum

        #region Property
        /// <summary>
        /// A name to identify the subscription, like "Spotify", "Netflix", etc.
        /// </summary>
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// A category for the subscription, like "phone", "Streaming", etc.
        /// </summary>
        public string Category
        {
            get { return category; }
            set
            {
                category = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// The price of the subscription, according the period
        /// </summary>
        public decimal Price
        {
            get { return price; }
            set
            {
                price = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// The frequency of billing the subscription
        /// </summary>
        public BillingPeriod Period
        {
            get { return period; }
            set
            {
                period = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// The address, link, or somethings else to have an indication where the subscription is
        /// </summary>
        public string Address
        {
            get { return address; }
            set
            {
                address = value;
                try
                {
                    AddressUri = new UriBuilder(address).Uri;
                }
                catch (UriFormatException)
                {
                    AddressUri = null;
                }

                OnPropertyChanged();
            }
        }
        /// <summary>
        /// The <see cref="Address"/> at the URI format. Is <see cref="null"/> if the address can't be cast to URI, this property will be null
        /// </summary>
        /// <remarks>Setted when set <see cref="Address"/></remarks>
        [XmlIgnore]
        public Uri AddressUri { get; private set; }
        /// <summary>
        /// The first day of the subscription, when it start
        /// </summary>
        public DateTime StartDate
        {
            get { return startDate; }
            set
            {
                startDate = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Is the subscription is active (need to be payed)
        /// </summary>
        public bool IsActive
        {
            get { return isActive; }
            set
            {
                isActive = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Is the subscription will be automaticly debited
        /// </summary>
        public bool IsAutomaticDebit
        {
            get { return isAutomaticDebit; }
            set
            {
                isAutomaticDebit = value;
                OnPropertyChanged();
            }
        }
        #endregion Property

        #region Methods
        /// <summary>
        /// Get the <see cref="Price"/> for the given period.
        /// For exemple : a annualy subscription at 120 will return 10 if perdiod is monthly
        /// </summary>
        /// <param name="period">Perdiod to get the value</param>
        /// <returns>The cost for the specified period</returns>
        public decimal PriceTo(BillingPeriod period)
        {
            decimal multiplier = 1;
            switch (period)
            {
                case BillingPeriod.Year:
                    if (this.period == BillingPeriod.Month) multiplier = 12.0m;
                    else if (this.period == BillingPeriod.Week) multiplier = 52.0m;
                    break;
                case BillingPeriod.Month:
                    if (this.period == BillingPeriod.Year) multiplier = 1.0m / 12.0m;
                    else if (this.period == BillingPeriod.Week) multiplier = 52.0m / 12.0m; // To year / nb month per year
                    break;
                case BillingPeriod.Week:
                    if (this.period == BillingPeriod.Year) multiplier = 1.0m / 52.0m;
                    else if (this.period == BillingPeriod.Month) multiplier = 12.0m / 52.0m; // to year / nb week per year
                    break;
                default:
                    break;
            }

            return Price * multiplier;
        }
        #endregion Methods

        #region Interface implementation
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(
               [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion Interface implementation

    }
}
