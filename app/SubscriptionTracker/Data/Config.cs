﻿using SubscriptionTracker.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;


namespace SubscriptionTracker.Data
{
    /// <summary>
    /// Configuration (settings) of the app
    /// </summary>
    [Serializable]
    public class Config
    {
        /// <summary> Position of the windows of the application </summary>
        public Point WindowPos { get; set; } = new Point();
        /// <summary> Size of the windows of the application </summary>
        public Size WindowSize { get; set; } = new Size();

        /// <summary>
        /// Take the windows size and position to save them in the the properties of this class
        /// </summary>
        /// <param name="window"></param>
        public void SaveWindowsState(MainWindow window)
        {
            WindowPos = new Point(window.Left, window.Top);
            WindowSize = new Size(window.Width, window.Height);
        }

        /// <summary>
        /// Load the properties of this class to restore the windows state (position and size)
        /// </summary>
        /// <param name="window"></param>
        public void LoadWindowState(MainWindow window)
        {
            window.Left = WindowPos.X;
            window.Top = WindowPos.Y;
            window.Width = WindowSize.Width;
            window.Height = WindowSize.Height;
        }
    }
}
