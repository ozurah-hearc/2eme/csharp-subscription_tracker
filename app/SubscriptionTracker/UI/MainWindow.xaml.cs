﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using SubscriptionTracker.Data;
using SubscriptionTracker.Manager;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SubscriptionTracker.UI
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Constant
        /// <summary>
        /// XML extension.
        /// </summary>
        private const string EXTENSION_XML = ".xml";

        /// <summary>
        /// XML description.
        /// </summary>
        private const string EXTENSION_XML_DESC = "XML Files";

        /// <summary>
        /// XML Filter for "FileDialog".
        /// </summary>
        private const string FILTER_FILEDIALOG_XML_AUTORIZED_EXT = EXTENSION_XML_DESC + "|*" + EXTENSION_XML + "|" + "Any file|*.*";

        /// <summary>
        /// Title of the window open data file (for XML).
        /// </summary>
        private readonly string OPEN_XML_TITLE = "Open subscription data file";

        /// <summary>
        /// Title of the window save data file (for XML).
        /// </summary>
        private readonly string SAVE_XML_TITLE = "Where to save subscription data file";
        #endregion Constant

        #region Field
        private Config settings;
        private FileData data;
        #endregion Field

        #region Property
        /// <summary>
        /// Values placed inside the DataGrid which contains each subscription (grouped by category).
        /// </summary>
        public ICollectionView ValuesForDataGrid { get; private set; }
        /// <summary>
        /// The data used by the chart pie for montly costs.
        /// </summary>
        /// <remarks>
        /// - Only 1 chart per SeriesCollection (can't bind it to 2 chart, else app crash or doesn't display on each chart).
        /// - It seems to have a bug when the chart content is updated, the binding doesn't trigg (but the content is correctly updated) (ie : the bug appears when using a converter).
        /// </remarks>
        public SeriesCollection SeriesCollectionPieMonthly { get; set; }

        /// <summary>
        /// Field for <see cref="SeriePieMonthlyHasData"/>.
        /// Considere using <see cref="SeriePieMonthlyHasData"/> instead
        /// </summary>
        private bool seriePieMonthlyHasData;
        /// <summary>
        /// Property used to set the visibility of the chart pie for monthly costs in bindings.
        /// </summary>
        /// <remarks>
        /// This way is a temporary solution, that should be replaced by a better one.
        /// The <see cref="SeriesCollection"/> seems to not properly works with bindings.
        /// </remarks>
        public bool SeriePieMonthlyHasData
        {
            get { return seriePieMonthlyHasData; }
            set
            {
                seriePieMonthlyHasData = value;
                OnPropertyChanged();
            }
        }
        #endregion Property

        #region Constructor
        public MainWindow()
        {
            InitializeComponent();
            settings = FileManager.ReadSettingsFile();
            if (settings == null)
                settings = new Config();
            else
            {
                settings.LoadWindowState(this);
            }

            data = new FileData();
            SeriesCollectionPieMonthly = new SeriesCollection();

            data.Subscriptions.CollectionChanged += Subscription_OnChange;

            // Add test data
            // data.Subscriptions.Add(new Subscription { Name = "Test", Category = "A" });
            // data.Subscriptions.Add(new Subscription { Name = "Test2", Category = "B" });
            // data.Subscriptions.Add(new Subscription { Name = "Test3", Category = "A" });

            // Update UI with default values
            UpdateDataGridList();
            pieMontlyCategorisedCost.DataContext = this;
            indicationOpenedFile.DataContext = data.XmlPath;
        }
        #endregion Constructor

        #region Method
        /// <summary>
        /// Update the data grid list (without an update the categories will not be updated).
        /// </summary>
        private void UpdateDataGridList()
        {
            ValuesForDataGrid = CollectionViewSource.GetDefaultView(data.Subscriptions);

            ValuesForDataGrid.GroupDescriptions.Clear();
            ValuesForDataGrid.GroupDescriptions.Add(new PropertyGroupDescription("Category"));
            ValuesForDataGrid.SortDescriptions.Add(new SortDescription("Category", ListSortDirection.Ascending));

            dataGrid.DataContext = ValuesForDataGrid;
        }

        /// <summary>
        /// Update the statistics of the subscriptions and the chart.
        /// The statistics are calculated using Linq, so this method just update the result.
        /// </summary>
        private void UpdateStats()
        {
            UpdateChartPeriodCostCategory();

            var validData = data.Subscriptions
                            .Where(item => item.IsActive
                                    && item.StartDate <= DateTime.Today.AddMonths(1));

            var validDataDirectDebit = validData.Where(item => item.IsAutomaticDebit);

            nextCost.DataContext = GetThisMonthCost(validData);
            nextCostDirectDebit.DataContext = GetThisMonthCost(validDataDirectDebit);

            numberRepartition.DataContext = new Tuple<int, int, int, int>(
                validData.Count(item => item.Period == Subscription.BillingPeriod.Week),
                validData.Count(item => item.Period == Subscription.BillingPeriod.Month),
                validData.Count(item => item.Period == Subscription.BillingPeriod.Year),
                validData.Count(item => item.Period == Subscription.BillingPeriod.Unspecified)
            );

            int nbFutureMonth = 3;
            futureSubscriptionDataGrid.DataContext = data.Subscriptions
                .Where(item => item.StartDate >= DateTime.Today
                        && item.StartDate <= DateTime.Today.AddMonths(nbFutureMonth))
                .OrderBy(item => item.StartDate);
        }

        /// <summary>
        /// Get the cost for the current month by using Linq query
        /// </summary>
        /// <param name="source">The source used for the calculation</param>
        private decimal GetThisMonthCost(IEnumerable<Subscription> source)
        {
            return source.Where(item => item.Period != Subscription.BillingPeriod.Year)
                .Sum(item => item.PriceTo(Subscription.BillingPeriod.Month))
                +
                source
                .Where(item => item.Period == Subscription.BillingPeriod.Year)
                .Sum(item => item.Price);
        }

        /// <summary>
        /// Update the chart for the monthly costs.
        /// </summary>
        /// <remarks>
        /// For some reasons, the chart is not updated when the SerieCollection is updated.
        /// So we force to update it by clearing it and re-adding the data.
        /// </remarks>
        private void UpdateChartPeriodCostCategory()
        {
            SeriesCollectionPieMonthly.Clear();

            var categorised = data.Subscriptions.Where(item => item.IsActive).GroupBy(item => item.Category)
                             .Select(group => new { category = group.Key, subscription = group.ToList() })
                             .ToList();

            foreach (var cat in categorised)
            {
                decimal totalPrice = cat.subscription.Sum(item => item.PriceTo(Subscription.BillingPeriod.Month));

                if (totalPrice > 0)
                {
                    // We have only 1 value per series : the total price

                    SeriesCollectionPieMonthly.Add(new PieSeries
                    {
                        Title = cat.category,
                        Values = new ChartValues<decimal> { totalPrice }
                    });
                }
            }

            SeriePieMonthlyHasData = SeriesCollectionPieMonthly.Count > 0;
        }

        /// <summary>
        /// Save the data at the actual data xml path
        /// If no actual path, it will call <see cref="SaveAs"/> before
        /// </summary>
        private void Save()
        {
            if (data != null)
            {
                // Save as
                if (string.IsNullOrEmpty(data?.XmlPath)) // Cause the user can cancel the filelocation
                {
                    SaveAs();
                }

                // Save
                if (!string.IsNullOrEmpty(data?.XmlPath)) // Cause the user can cancel the filelocation
                {
                    FileManager.WriteDataFile(data.XmlPath, data);
                    data.IsSaved = true;
                }
            }
            else
            {
                Console.WriteLine("There is no data to save");
            }
        }

        /// <summary>
        /// Prompt a save file dialog to the user to choose where to save the datas
        /// </summary>
        private void SaveAs()
        {
            if (data != null)
            {
                var file = FileManager.SaveAsFileDialog(SAVE_XML_TITLE, FILTER_FILEDIALOG_XML_AUTORIZED_EXT, EXTENSION_XML);
                if (file != null)
                {
                    data.XmlPath = file.FileName;
                    indicationOpenedFile.DataContext = data.XmlPath; // For some reasons, without redifine it, the datacontext isn't updated
                    Save();
                }
                else
                    Console.WriteLine("Canceled save as");

            }
            else
            {
                Console.WriteLine("There is no data to save");
            }
        }
        #endregion Method

        #region Event
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!data.IsSaved)
            {

                System.Diagnostics.Debug.WriteLine("unsaved");

                MessageBoxResult dialogResult = MessageBox.Show("The data was not saved, did you want to save it ?", "Save modified data ?", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    SaveAs(); // TODO the possibility to save it at the current location OR save as would be a nice option for the user
                    e.Cancel = !data.IsSaved; // If the data wasn't saved, the closing will be canceled; else it can closed
                }
                else if (dialogResult == MessageBoxResult.Cancel)
                {
                    e.Cancel = true;
                    return;
                }
            }

            settings.SaveWindowsState(this);
            FileManager.WriteSettingsFile(settings);
        }

        private void OpenFile_Click(object sender, RoutedEventArgs e)
        {
            var file = FileManager.OpenFileDialog(OPEN_XML_TITLE, FILTER_FILEDIALOG_XML_AUTORIZED_EXT);
            if (file != null)
            {
                try
                {
                    data = FileManager.ReadDataFile(file.FileName);
                    indicationOpenedFile.DataContext = data.XmlPath;
                    UpdateDataGridList();
                }
                catch (InvalidOperationException) // Exception type when deserialisation didn't work
                {
                    MessageBox.Show("The file you tried to open isn't supported by the application", "Cannot open file", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
                Console.WriteLine("Load not done : File not opened");
        }

        private void SaveFile_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void SaveAsFile_Click(object sender, RoutedEventArgs e)
        {
            SaveAs();
        }

        private void MenuItemAbout_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow aboutWindow = new AboutWindow
            {
                Owner = this
            };
            aboutWindow.ShowDialog();
        }

        private void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid.CurrentCell.Item != null && dataGrid.CurrentCell.Item.GetType() == typeof(Subscription)) //Si'il s'agit vraiment d'une ligne de compte (Pas une ligne spécial tel que l'ajout d'une nouvelle entrée
            {
                data.Subscriptions.Remove((Subscription)dataGrid.CurrentCell.Item);
                data.IsSaved = false;
            }
        }

        private void AddItem_Click(object sender, RoutedEventArgs e)
        {
            Subscription newSub = new Subscription { Category = "New" };
            data.Subscriptions.Add(newSub);

            data.IsSaved = false;

            //dataGrid.SelectedItem = newSub; // This one is binded to the "next price"; so we don't override the binding
            futureSubscriptionDataGrid.SelectedItem = newSub; // We select it here instead

            dataGrid.UpdateLayout();

            if (dataGrid.SelectedItem != null)
                dataGrid.ScrollIntoView(dataGrid.SelectedItem);
        }

        private void TextBox_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            int incr = 1;
            if (e.Delta < 0)
                incr = -1;

            if (dataGrid.SelectedItem is Subscription selected)
                selected.Price += incr;
        }

        private void Subscription_OnChange(object sender, NotifyCollectionChangedEventArgs e)
        {
            // Source : https://stackoverflow.com/questions/1427471/observablecollection-not-noticing-when-item-in-it-changes-even-with-inotifyprop

            // Add event for the new elements
            if (e.NewItems != null)
            {
                foreach (Object item in e.NewItems)
                {
                    ((INotifyPropertyChanged)item).PropertyChanged += SubscriptionItem_OnChanged;
                }
            }

            // Remove event for olds elements
            if (e.OldItems != null)
            {
                foreach (Object item in e.OldItems)
                {
                    ((INotifyPropertyChanged)item).PropertyChanged -= SubscriptionItem_OnChanged;
                }
            }

            UpdateStats();
        }

        private void SubscriptionItem_OnChanged(object sender, PropertyChangedEventArgs e)
        {
            data.IsSaved = false;

            // Only the "category" changed need to update the elements
            // Others elements works correctly with the binding
            if (e.PropertyName == nameof(Subscription.Category))
            {
                UpdateDataGridList();
            }

            // Update the stats (will update the Linq results)
            UpdateStats();
        }
        #endregion Event

        #region Interface implementation
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(
               [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion Interface implementation
    }
}
