﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using LiveCharts;

namespace SubscriptionTracker.Converter
{

    /// <summary>
    /// Converter for bindings : allows the conversion of a live chart serie count to visibility (>0 = Visibility.Collapsed / Null or 0 = Visibility.Visible).
    /// If it's not a live chart serie, the visiblity will be Visible.
    /// live chart serie <see cref="SeriesCollection"/>
    /// </summary>
    //https://stackoverflow.com/questions/21939667/nulltovisibilityconverter-make-visible-if-not-null
    public class EmptyChartToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SeriesCollection serie = value as SeriesCollection;

            if (serie == null)
                return Visibility.Visible;

            return serie.Count > 0 ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
