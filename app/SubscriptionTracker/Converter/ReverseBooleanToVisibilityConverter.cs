﻿using System;
using System.Windows;
using System.Windows.Data;

namespace SubscriptionTracker.Converter
{
    /// <summary>
    /// Converter for bindings : allows the conversion of a boolean to visibility
    /// Works the opposite of the existing converter "BooleanToVisibilityConverter" (True = Visibility.Collapsed / False = Visibility.Visible)
    /// </summary>
    class ReverseBooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
