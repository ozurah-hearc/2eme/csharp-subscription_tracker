﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubscriptionTracker
{
    /// <summary>
    /// Utility class, provides useful methods
    /// </summary>
    class Utility
    {
        /// <summary>
        /// if value is null, return false
        /// </summary>
        /// <param name="value">bool? to check</param>
        /// <returns></returns>
        public static bool BoolNullToFalse(dynamic value)
        {
            return value ?? false;
        }
    }
}
