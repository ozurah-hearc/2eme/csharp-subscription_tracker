﻿using Microsoft.Win32;
using SubscriptionTracker.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Xml.Serialization;

namespace SubscriptionTracker.Manager
{
    /// <summary>
    /// Class for the management of files (read, write)
    /// </summary>
    class FileManager
    {
        private const string SETTINGS_XML_FILE = "settings.xml";

        #region Static method
        /// <summary>
        /// Read and deserialize the data to xml file
        /// </summary>
        /// <param name="fileAndPath">File with path to read</param>
        /// <returns>Deserialized data</returns>
        public static FileData ReadDataFile(string fileAndPath)
        {
            FileData result = Deserialize<FileData>(fileAndPath);
            result.XmlPath = fileAndPath;
            return result;
        }

        /// <summary>
        /// Write the data to xml file to the disk
        /// </summary>
        /// <param name="fileAndPath">File with path to write</param>
        /// <param name="data">data to serialize</param>
        public static void WriteDataFile(string fileAndPath, FileData data)
        {
            Serialize(data, fileAndPath);
        }

        /// <summary>
        /// Read and deserialize the settings xml file (in the root of the app)
        /// Return null if file does not exist
        /// </summary>
        /// <returns>Deserialized data, null if file does not exist</returns>
        public static Config ReadSettingsFile()
        {
            string file = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, SETTINGS_XML_FILE);
            Config result = null;
            if (File.Exists(file))
                result = Deserialize<Config>(file);

            return result;
        }

        /// <summary>
        /// Write the settings file to the root location of the app file to the disk
        /// </summary>
        /// <param name="data">data to serialize</param>
        public static void WriteSettingsFile(Config data)
        {
            Serialize(data, Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, SETTINGS_XML_FILE));
        }

        /// <summary>
        /// Serialize object to XML file
        /// </summary>
        /// <param name="itemToSerialize">object who will be serialized</param>
        /// <param name="fileAndPath">File to write serialized data</param>
        private static void Serialize(object itemToSerialize, string fileAndPath)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(fileAndPath)); //Create folder if does not exist
            XmlSerializer xs = new XmlSerializer(itemToSerialize.GetType());
            using (StreamWriter wr = new StreamWriter(fileAndPath))
            {
                xs.Serialize(wr, itemToSerialize);
            }
        }

        /// <summary>
        /// Deserialize a XML file to object
        /// </summary>
        /// <typeparam name="T">Serialized object class</typeparam>
        /// <param name="fileAndPath">File to deserialize</param>
        /// <returns>Deserialized data</returns>
        private static T Deserialize<T>(string fileAndPath) where T : class
        {
            XmlSerializer xs = new XmlSerializer(typeof(T));

            using (StreamReader sr = new StreamReader(fileAndPath))
            {
                return (T)xs.Deserialize(sr);
            }

        }

        /// <summary>
        /// Open a file using the Windows Open File Dialog
        /// </summary>
        /// <param name="title">Title of the window</param>
        /// <param name="fileDialogAutorizedExtension">Which extensions are allowed</param>
        /// <returns>opened file (<see langword="null"/> if no file)</returns>
        public static OpenFileDialog OpenFileDialog(string title, string fileDialogAutorizedExtension)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = title;
            ofd.Filter = fileDialogAutorizedExtension;
            ofd.ShowDialog();
            return !string.IsNullOrEmpty(ofd.FileName) ? ofd : null; //if null = ie: canceled
        }

        /// <summary>
        /// Prompt a save file dialog to the user
        /// </summary>
        /// <param name="title">Title of the file dialog</param>
        /// <param name="fileDialogAutorizedExtension">Extension that can be used for the saved file</param>
        /// <param name="defaultExt">The default selected extension</param>
        /// <returns>The file dialogs. Null if nothing was opened</returns>
        public static SaveFileDialog SaveAsFileDialog(string title, string fileDialogAutorizedExtension, string defaultExt)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = title;
            sfd.AddExtension = true;
            sfd.Filter = fileDialogAutorizedExtension;
            sfd.DefaultExt = defaultExt;
            sfd.ShowDialog();
            return !string.IsNullOrEmpty(sfd.FileName) ? sfd : null; //if null = ie: canceled
        }

        #endregion Static method
    }
}
