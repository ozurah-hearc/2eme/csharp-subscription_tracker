﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;

namespace SubscriptionTracker
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {

            // Source for culture : https://social.msdn.microsoft.com/Forums/vstudio/en-US/d35e0a76-0ab0-44ba-8d8c-892bd2560542/default-cultureinfo-for-wpf?forum=wpf
            
            // To force a culture uncomment this
            //Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("fr-CH");
            //Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fr-CH");

            FrameworkElement.LanguageProperty.OverrideMetadata(
              typeof(FrameworkElement),
              new FrameworkPropertyMetadata(
                    XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag))
              );

            base.OnStartup(e);
        }
    }
}
